/**
 * Created by Александр on 9/3/2017.
 */
import userReducer, { ADD_USER, EDIT_USER, DELETE_USER } from '../ducks/user';

const initialState = {
  users: [],
};

const state = {
  users: [
    {
      id: 1,
      name: 'name',
      birthDate: 'birthDate',
      address: 'address',
      city: 'city',
      telephone: 'telephone',
    },
  ],
};

const user = {
  id: 1,
  name: 'name',
  birthDate: 'birthDate',
  address: 'address',
  city: 'city',
  telephone: 'telephone',
};

describe('userReducer test', () => {
  it('default state', () => {
    expect(
      userReducer(initialState, { type: undefined, payload: user })
    ).toEqual(initialState);
  });

  it('ADD_USER', () => {
    const action = {
      payload: user,
      type: ADD_USER,
    };
    expect(userReducer(initialState, action)).toEqual(state);
  });

  it('EDIT_USER', () => {
    const action = {
      type: EDIT_USER,
      payload: user,
    };

    const oldState = {
      users: [
        {
          id: 1,
          name: 'nameOld',
          birthDate: 'birthDateOld',
          address: 'addressOld',
          city: 'cityOld',
          telephone: 'telephoneOld',
        },
      ],
    };

    expect(userReducer(oldState, action)).toEqual(state);
  });

  it('DELETE_USER', () => {
    const action = {
      type: DELETE_USER,
      payload: user,
    };
    expect(userReducer(state, action)).toEqual(initialState);
  });
});
