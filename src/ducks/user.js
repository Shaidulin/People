import { generateId } from './utils';

const initialState = {
  users: [],
};

export const moduleName = 'add';
export const appName = 'people';
export const ADD_USER = `${appName}/${moduleName}/ADD_USER`;
export const EDIT_USER = `${appName}/${moduleName}/EDIT_USER`;
export const DELETE_USER = `${appName}/${moduleName}/DELETE_USER`;

export default function reducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case ADD_USER:
      return { ...state, users: state.users.concat([payload]) };

    case EDIT_USER:
      return {
        ...state,
        users: state.users.map((user) => user.id === payload.id ? Object.assign({}, user, payload) : user)
      }

    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter(user => user.id !== payload.id)
      }

    default:
      return state;
  }
}
export function addUser(newUser) {
  const id = generateId();

  return {
    type: ADD_USER,
    payload: Object.assign({}, newUser, { id })
  };
}

export function editUser(user) {
  return {
    type: EDIT_USER,
    payload: user,
  };
}

export function deleteUser(id) {
  return {
    type: DELETE_USER,
    payload: { id },
  };
}
