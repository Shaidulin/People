import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import userReducer, { moduleName as userModule } from '../ducks/user';

export default combineReducers({
  router,
  form,
  [userModule]: userReducer,
});
