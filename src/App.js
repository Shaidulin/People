import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Root from './components/Root';
import store from './redux';
import history from './history';

class App extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <MuiThemeProvider>
            <ConnectedRouter history={history}>
              <Root />
            </ConnectedRouter>
          </MuiThemeProvider>
        </Provider>
      </div>
    );
  }
}
export default App;
