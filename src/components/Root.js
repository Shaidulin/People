import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import UserPage from './routes/UserPage';

class Root extends Component {
  render() {
    return (
      <div>
        <Route path="/" component={UserPage} />
      </div>
    );
  }
}

export default Root;
