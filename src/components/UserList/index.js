import React from 'react';
import { Link } from 'react-router-dom';
import Delete from 'material-ui/svg-icons/action/delete-forever';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import PropTypes from 'prop-types';

function UserList(props) {
  return (
    <div>
      {props.users.map((elem) => {
        const actions = (
          <div>
            <div
              style={{
                display: 'inline-block',
                width: '50px',
                marginTop: '35px',
              }}
            >
              <p> {elem.name}</p>
            </div>
            <div
              style={{
                display: 'inline-block',
                width: '50px',
                verticalAlign: 'top',
                marginLeft: '122px',
              }}
            >
              <IconButton>
                <Link
                  to={{
                    pathname: `/edituser/${elem.id}`,
                    state: { elem }
                  }}
                >
                  <Edit />
                </Link>
              </IconButton>
              <IconButton onClick={() => props.deleteUser(elem.id)}>
                <Delete />
              </IconButton>
            </div>
          </div>
        );
        return (
          <Card
            key={elem.id}
            style={{
              display: 'inline-block',
              width: '300px',
              verticalAlign: 'top',
              marginLeft: '22px',
              marginBottom: '22px'
            }}
          >
            <CardHeader
              title={actions}
              subtitle={elem.birthDate}
              titleStyle={{
                whiteSpace: 'nowrap',
              }}
            />
            <CardText>
              <Divider />
              <p>{elem.address}</p>
              <Divider />
              <p>{elem.city}</p>
              <Divider />
              <p>{elem.telephone}</p>
            </CardText>
          </Card>
        );
      })}
    </div>
  );
}

UserList.propTypes = {
  users: PropTypes.array,
};

export default UserList;
