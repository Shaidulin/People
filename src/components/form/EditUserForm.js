import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ErrorField from '../common/ErrorField';
import Modal from '../common/Modal';
import AddButton from '../common/AddButton';

class EditUserForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    invalid: PropTypes.bool,
    reset: PropTypes.bool,
  };

  render() {
    const { handleSubmit, invalid, reset } = this.props;
    return (
      <div>
        <Modal open title="Изменить пользователя" dialogButton={<AddButton />}>
          <form onSubmit={handleSubmit} invalid={invalid} reset={reset}>
            <div>
              <label>ФИО(обязательно, не больше 100 символов)</label>
              <Field name="name" component={ErrorField} />
            </div>
            <div>
              <label>Дата рождения(обязательно, в форме трех селектов)</label>
              <Field name="birthDate" type='date' component={ErrorField} />
            </div>
            <div>
              <label>Адрес (одной строкой)</label>
              <Field name="address" component={ErrorField} />
            </div>
            <div>
              <label>Город</label>
              <Field name="city" component={ErrorField} />
            </div>
            <div>
              <label>Телефон (российский мобильный)</label>
              <Field name="telephone" type="number" component={ErrorField} />
            </div>
          </form>
        </Modal>
      </div>
    );
  }
}

const validate = ({
  name, birthDate, address, city, telephone,
}) => {
  const errors = {};
  if (!name) errors.name = 'Добавьте полное имя';
  else if (name.length > 100) errors.name = 'Превышен лимит количества символов';
  if (!birthDate) errors.birthDate = 'Заполните поле';
  if (!address) errors.address = 'Заполните поле';
  if (!city) errors.city = 'Заполните поле';
  if (!telephone) errors.telephone = 'Заполните поле';
  else if (
    telephone.length !== 10 &&
    !telephone.match(/(948|927|96[0-8]|91[1-9]|98[0-9]|92[1-9]|90[0-9]|95[0-8]|99[1-9]|977)\d{7}/gm)
  ) {
    errors.telephone =
      'Неправильный формат мобильного номера телефона.Формат:9XXXXXXXXX';
  }
  return errors;
};

EditUserForm = reduxForm({
  form: 'editUser',
  validate,
})(EditUserForm);

EditUserForm = connect(state => ({
  initialValues: state.router.location.state.elem,
  enableReinitialize: true,
}))(EditUserForm);


export default EditUserForm;
