import React from 'react';
import PropTypes from 'prop-types';

function ErrorField(props) {
  const { input, type, meta: { error, touched } } = props;
  const errorText = touched &&
  error && <div style={{ color: 'red' }}>{error}</div>;
  return (
    <div>
      <input {...input} type={type} />
      {errorText}
    </div>
  );
}

ErrorField.propTypes = {
  input: PropTypes.object,
  type: PropTypes.string,
  meta: PropTypes.object,
  error: PropTypes.string,
  touched: PropTypes.bool,
};

export default ErrorField;
