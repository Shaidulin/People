import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class Modal extends React.Component {
  static propTypes = {
    children: PropTypes.object,
    dialogButton: PropTypes.node,
  };
  state = {
    open: false,
  };
  handleCloseSubmit = (e) => {
    this.props.children.props.onSubmit(e);
    this.props.children.props.reset();
    this.setState({ open: false });
  };
  handleOpen = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const action = (
      <div>
        <FlatButton
          onClick={e => this.handleCloseSubmit(e)}
          type="submit"
          disabled={this.props.children.props.invalid}
          label="Submit"
        />
        <Link
          to={{
            pathname: '/',
          }}
        >
          <FlatButton onClick={e => this.handleClose(e)} label="Cansel" />
        </Link>
      </div>
    );

    return (
      <div>
        {this.props.open || (<div onClick={this.handleOpen}>{this.props.dialogButton}</div>)}
        <Dialog
          title={this.props.title}
          modal
          actions={action}
          open={this.props.open || this.state.open}
        >
          {this.props.children}
        </Dialog>
      </div>
    );
  }
}

Modal.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
};

export default withRouter(Modal);
