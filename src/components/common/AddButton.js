import React from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { addButtonBackgroundColor } from '../../config';

export default () => (
  <FloatingActionButton mini backgroundColor={addButtonBackgroundColor}>
    <ContentAdd />
  </FloatingActionButton>
);
