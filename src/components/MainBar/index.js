import React from 'react';
import AppBar from 'material-ui/AppBar';
import PropTypes from 'prop-types';
import { addBarBackgroundColor } from '../../config';

function MainBar(props) {
  return (
    <AppBar
      style={{ marginBottom: '22px', backgroundColor: addBarBackgroundColor }}
      title={<span>Directory of people</span>}
      iconElementLeft={props.elemLeft}
    />
  );
}

MainBar.propTypes = {
  elemLeft: PropTypes.node,
};
export default MainBar;
