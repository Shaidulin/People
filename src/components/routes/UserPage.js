import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addUser, moduleName, editUser, deleteUser } from '../../ducks/user';
import AddUserForm from '../form/AddUserForm';
import EditUserForm from '../form/EditUserForm';
import UserList from '../UserList';
import MainBar from '../MainBar';

class UserPage extends Component {

  static propTypes = {
    addUser: PropTypes.func,
    history: PropTypes.object,
    editUser: PropTypes.func,
    deleteUser: PropTypes.func,
    users: PropTypes.array,
  };

  handleAddUser = (user) => {
    this.props.addUser(user);
    this.props.history.push('/');
  };



  handleEditUser = (elem) => {
    this.props.editUser(elem);
    this.props.history.push('/');
  };

  render() {
    return (
      <div>
        <Route
          path="/"
          render={() => (
            <MainBar elemLeft={<AddUserForm onSubmit={this.handleAddUser} />} />
          )}
        />
        <Route
          path="/edituser/:id"
          render={id => (
            <EditUserForm match={id} onSubmit={this.handleEditUser} />
          )}
        />
        <Route
          path="/"
          render={() => (
            <UserList
              deleteUser={this.props.deleteUser}
              users={this.props.users}
            />
          )}
        />
      </div>
    );
  }
}

export default connect(state => ({ users: state[moduleName].users, state }), {
  addUser,
  editUser,
  deleteUser,
})(UserPage);
