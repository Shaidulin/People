Небольшое CRUD-приложение для ведения данных пользователей.

У пользователя есть:

* ФИО (обязательно, не больше 100 символов) 
* Дата рождения (обязательно, в форме трех селектов) 
* Адрес (одной строкой) 
* Город 
* Телефон (российский мобильный)

Приложение — список пользователей с этими данными и возможностью их создавать/менять/удалять. 
Для формы добавления/обновления пользователей нужно добавить валидации с выводом ошибок. 
Чтобы данные не исчезали при обновлении страницы, нужно сохранять их в localStorage.

# Installation

```bash
git clone git@gitlab.com:Shaidulin/People.git
npm install
```

# Development

```bash
npm start
localhost:3000
```